use serde::{Deserialize, Serialize};
use snafu::Snafu;
use std::convert::TryFrom;
use std::fmt::{Display, Formatter};
use std::str::FromStr;

// Eq and Hash are required for the integration tests

#[derive(Clone, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub struct TransactionId {
    value: Vec<u8>,
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize, Snafu)]
pub enum TransactionIdError {
    #[snafu(display(
        "The expected Transaction Id format is '0x' followed by 64 hex characters. value={}",
        value
    ))]
    Parsing { value: String },

    #[snafu(display(
        "Cannot create a TransactionId from the supplied bytes. Must be a byte array with a length of 32 bytes."
    ))]
    Encoding { input: Vec<u8> },
}

impl TransactionId {
    pub fn parse(input: String) -> Result<TransactionId, TransactionIdError> {
        let rx: regex::Regex = regex::Regex::new("^(0x)?[0-9|A-F|a-f]{64}$").unwrap();

        if !rx.is_match(&input) {
            return Err(TransactionIdError::Parsing { value: input });
        }

        let body = match input.len() {
            64 => input, // came in without header bytes
            66 => {
                // came in with header bytes. Let's strip them off.
                let (_, body) = input.split_at(2);
                body.to_string()
            }
            _ => "not possible".to_string(),
        };

        // this is safe to do since we've already validated the
        // string is valid hex characters.
        let value = hex::decode(body).unwrap();

        Ok(TransactionId { value })
    }

    pub fn decode(&self) -> Vec<u8> {
        self.value.clone()
    }
}

impl Display for TransactionId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let value_str = hex::encode_upper(&self.value);
        write!(f, "0x{}", value_str)
    }
}

impl FromStr for TransactionId {
    type Err = TransactionIdError;

    fn from_str(input: &str) -> Result<TransactionId, Self::Err> {
        TransactionId::parse(input.to_string())
    }
}

impl TryFrom<String> for TransactionId {
    type Error = TransactionIdError;
    fn try_from(input: String) -> Result<TransactionId, Self::Error> {
        TransactionId::parse(input)
    }
}

impl TryFrom<Vec<u8>> for TransactionId {
    type Error = TransactionIdError;

    fn try_from(input: Vec<u8>) -> Result<TransactionId, Self::Error> {
        if input.len() != 32 {
            return Err(TransactionIdError::Encoding { input });
        }

        Ok(TransactionId {
            value: input.clone(),
        })
    }
}

impl From<[u8; 32]> for TransactionId {
    fn from(input: [u8; 32]) -> TransactionId {
        TransactionId {
            value: input.to_vec(),
        }
    }
}

impl From<&[u8; 32]> for TransactionId {
    fn from(input: &[u8; 32]) -> TransactionId {
        TransactionId {
            value: input.to_vec(),
        }
    }
}

impl From<TransactionId> for [u8; 32] {
    fn from(input: TransactionId) -> [u8; 32] {
        let mut array = [0; 32];
        array.copy_from_slice(&input.value);
        array
    }
}

impl From<TransactionId> for Vec<u8> {
    fn from(input: TransactionId) -> Vec<u8> {
        input.value.clone()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::convert::TryInto;

    #[test]
    fn str_try_into() {
        let input =
            "0x1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF".to_string();
        let id: TransactionId = input.clone().try_into().unwrap();
        assert_eq!(id.to_string(), input);
    }

    #[test]
    fn from_str() {
        let input = "0x1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF";
        let id: TransactionId = TransactionId::from_str(&input).unwrap();
        assert_eq!(id.to_string(), input);
    }

    #[test]
    fn to_from_u8_32() {
        let input: [u8; 32] = [
            18, 52, 86, 120, 144, 171, 205, 239, 18, 52, 86, 120, 144, 171, 205, 239, 18, 52, 86,
            120, 144, 171, 205, 239, 18, 52, 86, 120, 144, 171, 205, 239,
        ];

        let id: TransactionId = input.into();

        let output: [u8; 32] = id.into();
        assert_eq!(input, output);
    }

    #[test]
    fn to_from_vec_u8() {
        let input: Vec<u8> = vec![
            18, 52, 86, 120, 144, 171, 205, 239, 18, 52, 86, 120, 144, 171, 205, 239, 18, 52, 86,
            120, 144, 171, 205, 239, 18, 52, 86, 120, 144, 171, 205, 239,
        ];

        let id: TransactionId = input.clone().try_into().unwrap();

        let output: Vec<u8> = id.into();
        assert_eq!(input, output);
    }

    #[test]
    pub fn parse() {
        let input = "0x1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF";

        let id = TransactionId::parse(input.to_string()).unwrap();

        assert_eq!(id.to_string(), input);
    }

    #[test]
    pub fn parse_case_insensitive() {
        let input = "0x1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef";

        let id = TransactionId::parse(input.to_string()).unwrap();

        assert_eq!(
            id.to_string(),
            "0x1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF"
        );
    }

    #[test]
    pub fn parse_missing_header() {
        let input = "1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF";

        let id = TransactionId::parse(input.to_string()).unwrap();

        assert_eq!(
            id.to_string(),
            "0x1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF"
        );
    }

    #[test]
    pub fn arbitrary_string() {
        let id = TransactionId::parse("fredbob".to_string());

        assert_eq!(
            id,
            Err(TransactionIdError::Parsing {
                value: "fredbob".to_string()
            })
        );
    }

    #[test]
    pub fn parse_too_short() {
        let input = "0x1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF1234567890ABCDE";

        let id = TransactionId::parse(input.to_string());

        assert_eq!(
            id,
            Err(TransactionIdError::Parsing {
                value: input.to_string()
            })
        );
    }

    #[test]
    pub fn parse_too_long() {
        let input = "0x1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF0";

        let id = TransactionId::parse(input.to_string());

        assert_eq!(
            id,
            Err(TransactionIdError::Parsing {
                value: input.to_string()
            })
        );
    }

    #[test]
    pub fn parse_non_hex_character() {
        let input = "0x1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF1234567890ABCDEG";

        let id = TransactionId::parse(input.to_string());

        assert_eq!(
            id,
            Err(TransactionIdError::Parsing {
                value: input.to_string()
            })
        );
    }

    #[test]
    pub fn decode() {
        let input = "0x1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF";

        let id = TransactionId::parse(input.to_string()).unwrap();

        let decoded = id.decode();

        // 0x12 = 1 * 16 + 2 = 18, etc.
        let expected = vec![
            18, 52, 86, 120, 144, 171, 205, 239, 18, 52, 86, 120, 144, 171, 205, 239, 18, 52, 86,
            120, 144, 171, 205, 239, 18, 52, 86, 120, 144, 171, 205, 239,
        ];

        assert_eq!(decoded, expected);
    }
}
