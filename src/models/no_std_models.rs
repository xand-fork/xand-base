/// IPv4 CIDR Block, with first for indices the ip address and last being the suffix
pub type CidrBlock = [u8; 5];
