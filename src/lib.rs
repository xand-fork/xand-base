#![cfg_attr(not(feature = "std"), no_std)]

//! This crate exists to provide the most base-level shared code. This means it should never depend
//! on anything else we write, and should be relatively small.
#[cfg(feature = "std")]
#[macro_use]
extern crate strum_macros;
#[cfg(feature = "std")]
#[macro_use]
extern crate derive_more;

pub mod models;
#[cfg(feature = "std")]
pub mod procutils;
#[cfg(feature = "std")]
pub mod snafu_extensions;
#[cfg(feature = "std")]
pub mod timeout;
#[cfg(feature = "std")]
pub mod utils;
